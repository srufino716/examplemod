package com.example.examplemod.infinity.gui;

import com.example.examplemod.infinity.blocks.TileEntityInfinityGenerator;
import com.example.examplemod.infinity.container.ContainerInfinityGenerator;
import com.example.examplemod.util.gui.LCGuiHandler;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiHandlerEntryInfinityGenerator extends LCGuiHandler.GuiHandlerEntry {

    public static final int ID = 51;

    @SideOnly(Side.CLIENT)
    @Override
    public GuiScreen getClientGui(EntityPlayer player, World world, int x, int y, int z) {
        return new GuiInfinityGenerator(player.inventory, (TileEntityInfinityGenerator) world.getTileEntity(new BlockPos(x, y, z)));
    }

    @Override
    public Container getServerContainer(EntityPlayer player, World world, int x, int y, int z) {
        return new ContainerInfinityGenerator(player.inventory, (TileEntityInfinityGenerator) world.getTileEntity(new BlockPos(x, y, z)));
    }
}
