package com.example.examplemod.extendedinventory.gui;

import com.example.examplemod.LCConfig;
import com.example.examplemod.extendedinventory.network.MessageOpenExtendedInventory;
import com.example.examplemod.network.LCPacketDispatcher;
import com.example.examplemod.proxies.LCClientProxy;
import com.example.examplemod.tabs.AbstractTab;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class InventoryTabExtendedInventory extends AbstractTab {

    public InventoryTabExtendedInventory() {
        super(0, 0, 0, new ItemStack(Items.IRON_CHESTPLATE));
    }

    @Override
    public void onTabClicked() {
        if (LCClientProxy.canClickTab())
            LCPacketDispatcher.sendToServer(new MessageOpenExtendedInventory());
    }

    @Override
    public boolean shouldAddToList() {
        return LCConfig.modules.extended_inventory;
    }
}
