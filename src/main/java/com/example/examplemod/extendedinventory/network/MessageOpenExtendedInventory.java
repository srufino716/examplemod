package com.example.examplemod.extendedinventory.network;

import io.netty.buffer.ByteBuf;
import com.example.examplemod.ExampleMod;
import com.example.examplemod.extendedinventory.gui.GuiHandlerEntryExtendedInventory;
import com.example.examplemod.network.AbstractServerMessageHandler;
import com.example.examplemod.util.gui.LCGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageOpenExtendedInventory implements IMessage {

    public MessageOpenExtendedInventory() {
    }

    @Override
    public void fromBytes(ByteBuf buf) {

    }

    @Override
    public void toBytes(ByteBuf buf) {

    }

    public static class Handler extends AbstractServerMessageHandler<MessageOpenExtendedInventory> {

        @Override
        public IMessage handleServerMessage(EntityPlayer player, MessageOpenExtendedInventory message, MessageContext ctx) {

            ExampleMod.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

                @Override
                public void run() {
                    LCGuiHandler.openGui(player, GuiHandlerEntryExtendedInventory.ID);
                }

            });

            return null;
        }

    }

}