package com.example.examplemod.extendedinventory.capabilities;

import com.example.examplemod.extendedinventory.InventoryExtendedInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public interface IExtendedInventoryCapability {

    public InventoryExtendedInventory getInventory();

    public NBTTagCompound writeNBT();

    public void readNBT(NBTTagCompound nbt);

    public void syncToPlayer();

    public void syncToPlayer(EntityPlayer receiver);

    public void syncToAll();

}
