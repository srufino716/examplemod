package com.example.examplemod.core;

import com.example.examplemod.sizechanging.ModuleSizeChanging;
import com.example.examplemod.sizechanging.capabilities.CapabilitySizeChanging;
import net.minecraft.entity.Entity;

public class LCHooks {

    public static void setSize(Entity entity, float width, float height) {
        if (ModuleSizeChanging.INSTANCE.isEnabled() && entity.hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
            entity.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).setOriginalSize(width, height);
    }
}
