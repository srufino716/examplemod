package com.example.examplemod.addonpacks;

import com.example.examplemod.ExampleMod;
import net.minecraftforge.common.config.Config;

@Config(modid = ExampleMod.MODID, name = "em_addonpacks")
public class AddonPackConfig {

    @Config.RequiresMcRestart
    public static String addonpacksFolder = "addonpacks";

}